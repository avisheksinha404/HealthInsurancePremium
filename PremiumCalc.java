//PremiumCalc.java
package com.business;

public class PremiumCalc {
	
	public double getPremOnAge(int age) {
		double base=getBasePremium();
		if(age>=18 && age <=40) {
			return (base+0.1*base);
		}
		if(age>=40) {
			base=base+0.2*base;
			return base;
		}
		
		return 0;
	}
	
	public double getBasePremium() {
		return 5000;
	}
	
	public double getPremOnGender(String gender) {
		double base=getBasePremium();
		if(gender.equalsIgnoreCase("MALE"))
			return base+0.02*base;
		else
			return base;
	}
	
	public double getPremOnHealth(boolean hypertension,boolean bloodPressure,boolean bloodSugar,boolean overweight) {
		
		double base=getBasePremium();
		if(hypertension)
			base=base+0.01*base;
		if(bloodPressure)
			base=base+0.01*base;
		if(bloodSugar)
			base=base+0.01*base;
		if(overweight)
			base=base+0.01*base;
		return base;
			
	}
	
	public double getPremOnHabits(boolean smoking,boolean alcohal,boolean excercise,boolean drugs) {
		
		double base=getBasePremium();
		if(excercise)
			base=base-0.03*base;
		if(smoking)
			base=base-0.03*base;
		if(alcohal)
			base=base-0.03*base;
		if(drugs)
			base=base-0.03*base;
		return base;
	}

}
