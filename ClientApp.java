//ClientApp.java
package com.client;

import java.util.Scanner;

import com.business.PremiumCalc;
import com.model.MemberDetails;

public class ClientApp {

	public static void main(String[] args) {
		MemberDetails md=new MemberDetails();
		Scanner sc=new Scanner(System.in);
		System.out.print("\nName: ");
		md.setName(sc.next());
		System.out.print("\nGender: ");
		md.setGender(sc.next());
		System.out.print("\nAge: ");
		md.setAge(sc.nextInt());
		System.out.print("\nCurrent Health: ");
		System.out.print("\nHypertension: ");
		String HT=sc.next();
		//System.out.println(md.health);
		if(HT.equalsIgnoreCase("No"))
			md.setHypertension(false);
		else
			md.setHypertension(true);
		
		System.out.print("\nBlood pressure: ");
		String BP=sc.next();
		if(BP.equalsIgnoreCase("No"))
			md.setBloodPressure(false);
		else
			md.setBloodPressure(true);
		
		System.out.print("\nBlood Sugar: ");
		String BS=sc.next();
		if(BS.equalsIgnoreCase("No"))
			md.setBloodSugar(false);
		else
			md.setBloodSugar(true);
		
		System.out.print("\nOverweight: ");
		String OW=sc.next();
		if(OW.equalsIgnoreCase("No"))
			md.setOverweight(false);
		else
			md.setOverweight(true);
		System.out.print("\nHabits: ");
		System.out.print("\nSmoking: ");
		String SW=sc.next();
		if(SW.equalsIgnoreCase("No"))
			md.setSmoking(false);
		else
			md.setSmoking(true);
		
		System.out.print("\nAlcohal: ");
		String AL=sc.next();
		if(AL.equalsIgnoreCase("No"))
			md.setAlcohal(false);
		else
			md.setAlcohal(true);
		
		System.out.print("\nExcercise: ");
		String EX=sc.next();
		if(EX.equalsIgnoreCase("No"))
			md.setExcercise(false);
		else
			md.setExcercise(true);
		
		System.out.print("\nDrugs: ");
		String DG=sc.next();
		if(DG.equalsIgnoreCase("No"))
			md.setDrugs(false);
		else
			md.setDrugs(true);
		
		PremiumCalc calc=new PremiumCalc();
		double p1=calc.getPremOnAge(md.getAge());
		double p2=calc.getPremOnGender(md.getGender());
		double p3=calc.getPremOnHealth(md.isHypertension(),md.isBloodPressure(),md.isBloodSugar(),md.isOverweight());
		double p4=calc.getPremOnHabits(md.isSmoking(), md.isAlcohal(), md.isExcercise(), md.isDrugs());
		
		double totalPrem=p1+p2+p3+p4;
		
		System.out.println("Health Insurance Premium for Mr. "+md.getName()+": Rs."+totalPrem);
		
		
	}

}
