//MemberDetails.java
package com.model;

import java.util.Scanner;

public class MemberDetails {
	
	private String name;
	private String gender;
	private int age;
	private boolean hypertension;
	private boolean bloodPressure;
	private boolean bloodSugar;
	private boolean overweight;
	private boolean smoking;
	private boolean alcohal;
	private boolean excercise;
	private boolean drugs;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public boolean isHypertension() {
		return hypertension;
	}
	public void setHypertension(boolean hypertension) {
		this.hypertension = hypertension;
	}
	public boolean isBloodPressure() {
		return bloodPressure;
	}
	public void setBloodPressure(boolean bloodPressure) {
		this.bloodPressure = bloodPressure;
	}
	public boolean isBloodSugar() {
		return bloodSugar;
	}
	public void setBloodSugar(boolean bloodSugar) {
		this.bloodSugar = bloodSugar;
	}
	public boolean isOverweight() {
		return overweight;
	}
	public void setOverweight(boolean overweight) {
		this.overweight = overweight;
	}
	public boolean isSmoking() {
		return smoking;
	}
	public void setSmoking(boolean smoking) {
		this.smoking = smoking;
	}
	public boolean isAlcohal() {
		return alcohal;
	}
	public void setAlcohal(boolean alcohal) {
		this.alcohal = alcohal;
	}
	public boolean isExcercise() {
		return excercise;
	}
	public void setExcercise(boolean excercise) {
		this.excercise = excercise;
	}
	public boolean isDrugs() {
		return drugs;
	}
	public void setDrugs(boolean drugs) {
		this.drugs = drugs;
	}
}